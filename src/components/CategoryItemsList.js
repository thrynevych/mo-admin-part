import React from 'react';

const CategoryItemsList = ({categoryItems}) => {
    console.log(categoryItems);
    return (
        <table className="highlight responsive-table centered">
            <thead>
            <tr>
                <th>№</th>
                <th>Item Name</th>
                <th>Item Price</th>
                <th>Item Price</th>
            </tr>
            </thead>
            <tbody>
            {categoryItems && categoryItems.map((item, index) => {
                return (
                    <tr key={item.id}>
                        <td>{index +1}</td>
                        <td>{item.name}</td>
                        <td>{item.price}</td>
                        <td>
                            <span className="item-actions">
                                <a href="#" className="align-left secondary-content"><i className="material-icons">edit</i></a>
                                <a href="#" className="align-left red-text text-darken-2 secondary-content"><i className="material-icons">delete</i></a>
                            </span>
                        </td>
                    </tr>

                )
            })}
            </tbody>
        </table>
    )
};

export default CategoryItemsList;
