import React from 'react';
import { Link } from 'react-router-dom';

const CategoriesList = ({categories}) => {
    console.log(categories);

    return categories && categories.map(item => {
        return (
            <Link to={`/category-items/${item.id}`} className="collection-item" key={item.id}>
                <div className="left-align teal-text text-darken-4">
                    {item.name}
                </div>
            </Link>
        )
    })
};

export default CategoriesList;
