import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const NavBar = () => {
    return (
        <nav className="nav-wrapper grey darken-3">
            <div className="container">
                <ul className="left">
                    <li><Link to='/' className="brand-logo">MO Admin</Link></li>
                </ul>
                <ul className="right">
                    <li><NavLink to='/'>Categories</NavLink></li>
                    <li><NavLink to='/'>Category items</NavLink></li>
                </ul>
            </div>
        </nav>
    )
};

export default NavBar;