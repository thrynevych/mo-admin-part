import React, {Component} from 'react';
import CategoriesList from './CategoriesList';

class Categories extends Component {
    categories = [
        {name: 'Category1', id: 1},
        {name: 'Category2', id: 2}
    ];
    render() {
        return(
            <div className="container">
                <ul className="collection with-header">
                    <li className="collection-header left-align">
                        <h4>Categories
                            <a className="waves-effect waves-light btn-small right">
                                <i className="material-icons right">add</i>
                                Add Category
                            </a>
                        </h4>
                    </li>
                    <CategoriesList categories={this.categories}/>
                </ul>
            </div>
        )
    }
}

export default Categories;