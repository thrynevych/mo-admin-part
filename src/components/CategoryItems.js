import React, {Component} from 'react';
import CategoryItemsList from "./CategoryItemsList";

class CategoryItems extends Component {
    categoryItems = [
        {id: '1', name: 'item1', price: 99.99, categoryId: 1},
        {id: '2', name: 'item2', price: 199.99, categoryId: 3},
        {id: '3', name: 'item3', price: 956.99, categoryId: 2}
    ];

    render() {
        return(
            <div className="container">
                <h3 className='left-align'>
                    Category items
                    <a className="waves-effect waves-light btn-small right">
                        <i className="material-icons right">add</i>
                        Add Item
                    </a>
                </h3>
                <CategoryItemsList categoryItems={this.categoryItems}/>
            </div>
        )
    }
}

export default CategoryItems;