import React, { Component } from 'react';
import './App.css';
import NavBar from './components/Navbar';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Categories from "./components/Categories";
import CategoryItems from "./components/CategoryItems";


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <NavBar />
          <Switch>
            <Route exact path='/' component={Categories} />
            <Route path='/category-items/:id' component={CategoryItems} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
